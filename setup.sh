kubectl -n kube-system create sa tiller
kubectl create clusterrolebinding tiller-cluster-rule \
    --clusterrole=cluster-admin \
    --serviceaccount=kube-system:tiller
helm init --skip-refresh --upgrade --service-account tiller
helm repo add fluxcd https://fluxcd.github.io/flux
kubectl apply -f https://raw.githubusercontent.com/fluxcd/flux/master/deploy-helm/flux-helm-release-crd.yaml
sleep 10
helm upgrade -i flux \
--set helmOperator.create=true \
--set helmOperator.createCRD=false \
--set git.url=git@gitlab.com:rohitshrivastava04/flux-sample.git \
--set git-poll-interval=10s \
--namespace flux \
fluxcd/flux