echo "delete flux"
helm delete --purge flux
echo "delete sample-app"
helm delete --purge sample-app
echo "delete traefik"
helm delete --purge traefik

echo "waiting ..."
sleep 3

echo "installing flux again"
helm upgrade -i flux \
--set helmOperator.create=true \
--set helmOperator.createCRD=false \
--set git.url=git@gitlab.com:rohitshrivastava04/flux-sample.git \
--set git-poll-interval=10s \
--namespace flux \
fluxcd/flux

echo "waiting ..."
sleep 3

fluxctl identity --k8s-fwd-ns flux